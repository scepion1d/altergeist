package de.unihd.pvs.AlterGeist.open.refine.ext.commands;

import com.google.refine.commands.Command;
import com.google.refine.model.Project;
import de.unihd.pvs.AlterGeist.open.refine.ext.Aggregator;
import de.unihd.pvs.AlterGeist.open.refine.ext.util.ResponseUtil;
import org.json.JSONWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AggregationPreviewCommand extends Command {
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Project project = getProject(request);

            int rowsNum = Integer.parseInt(request.getParameter("rowsNum"));
            int firstRow = Integer.parseInt(request.getParameter("firstRow"));

            String inputValuesString = request.getParameter("inputValues");
            if (inputValuesString == null) {
                respond(response, "{ \"code\" : \"error\", \"message\" : \"No input values specified\" }");
                return;
            }

            Aggregator aggregator = new Aggregator(project, inputValuesString, firstRow, rowsNum);

            response.setCharacterEncoding("UTF-8");
            response.setHeader("Content-Type", "application/json");

            JSONWriter writer = new JSONWriter(response.getWriter());
            writer.object();

            writer.key("code"); writer.value("ok");
            ResponseUtil.appendArrayToWriter("aggregatedData", aggregator.processData().toArray(new String[rowsNum]), writer);

            writer.endObject();
        } catch (Exception e) {
            respondException(response, e);
        }
    }

}


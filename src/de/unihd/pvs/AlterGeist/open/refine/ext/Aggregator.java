package de.unihd.pvs.AlterGeist.open.refine.ext;

import com.google.refine.model.Project;
import de.unihd.pvs.AlterGeist.open.refine.ext.util.BaseUtil;
import org.json.JSONException;

import java.util.List;
import java.util.Random;

public class Aggregator {
    private List<String> inputData;
    private List<List<String>> projectData;
    private int rowsNum;
    private int firstRow;

    /**
     * Aggregator constructor for partial processing of the project data
     *
     * @param project     Target project to process data
     * @param inputValues list of templates for aggregation
     * @param rowsNum     number of rows of the project to process (aggregator will process full project if rowsNum less than zero)
     * @throws JSONException
     */
    public Aggregator(Project project, String inputValues, int firstRow, int rowsNum) throws JSONException {
        this.firstRow = firstRow;
        this.rowsNum = rowsNum < 0
                ? project.rows.size()
                : rowsNum;

        projectData = BaseUtil.getProjectTablePart(project, firstRow, rowsNum);
        inputData = BaseUtil.convertJsonArrayIntoList(inputValues, rowsNum, "");
    }

    /**
     * Aggregator constructor for full processing of the project data
     *
     * @param project     Target project to process data
     * @param inputValues list of templates for aggregation
     * @throws JSONException
     */
    public Aggregator(Project project, String inputValues) throws JSONException {
        this.firstRow = 0;
        this.rowsNum = project.rows.size();

        this.projectData = BaseUtil.getProjectTablePart(project, rowsNum, firstRow);
        this.inputData = BaseUtil.convertJsonArrayIntoList(inputValues, rowsNum, "");
    }

    /**
     * Proxy method for data processing
     *
     * @return Aggregated data of specified part of project
     */
    public List<String> processData() {
        if (!listContainsOnlyEmpty(inputData)) {
            // Some test processing
            Random rnd = new Random();
            for (int i = 0; i < inputData.size(); i++) {
                if (inputData.get(i) == null || inputData.get(i).isEmpty()) {
                    inputData.set(i, String.valueOf(rnd.nextInt()));
                }
            }
        }
        return inputData;
    }

    /**
     * Check content of array
     *
     * @return True if list consists of empty strings or nulls
     */
    private boolean listContainsOnlyEmpty(List<String> list) {
        for (String value : list) {
            if (!value.isEmpty()) {
                return false;
            }
        }
        return true;
    }

}

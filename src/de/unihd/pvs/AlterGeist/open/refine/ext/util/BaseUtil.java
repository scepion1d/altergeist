package de.unihd.pvs.AlterGeist.open.refine.ext.util;

import com.google.refine.model.Cell;
import com.google.refine.model.Column;
import com.google.refine.model.Project;
import com.google.refine.model.Row;
import com.google.refine.util.ParsingUtilities;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class BaseUtil {

    /**
     * Convert part of the project table into matrix of strings
     *
     * @param project  target project
     * @param rowsNum  number of rows of the project to process (aggregator will process full project if rowsNum less than zero)
     * @param firstRow first row of the project to export
     * @return project data with header and specified number of rows
     */
    public static List<List<String>> getProjectTablePart(Project project, int firstRow, int rowsNum) {
        List<List<String>> dataTable = new ArrayList<List<String>>();

        if (firstRow < 0) { firstRow = 0; }
        if (rowsNum < 0) { rowsNum = project.rows.size() - 1; }

        int lastRow = firstRow + rowsNum - 1; // get id of last row
        if (lastRow >= project.rows.size()) {
            lastRow = project.rows.size() - 1;
        }

        // Extract data of each column
        for (int i = firstRow; i <= lastRow; i++) {
            List<String> rowData = new ArrayList<String>();
            Row row = project.rows.get(i);

            for (Column column : project.columnModel.columns) {
                Cell cell = row.getCell(column.getCellIndex());

                if (cell == null) {
                    rowData.add("");
                } else {
                    rowData.add(cell.value.toString());
                }
            }

            dataTable.add(rowData);
        }

        return dataTable;
    }

    /**
     * Convert the project table into matrix of strings
     *
     * @param project target project
     * @return project data with header and specified number of rows
     */
    public static List<List<String>> getFullProjectTable(Project project) {
        return getProjectTablePart(project, 0, -1);
    }

    /**
     * Convert json array into list with specified length
     *
     * @param jsonArrayString Json array string
     * @param length          Length of the result array
     * @param filler          String to fill empty values in list
     * @return List of strings with specified length based on specified json string
     * @throws JSONException
     */
    public static List<String> convertJsonArrayIntoList(String jsonArrayString, int length, String filler) throws JSONException {
        List<String> list = new ArrayList<String>();

        JSONArray jsonArray = ParsingUtilities.evaluateJsonStringToArray(jsonArrayString);

        if (jsonArray.length() != 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                String value = jsonArray.getString(i) == null || jsonArray.getString(i).isEmpty()
                        ? filler
                        : jsonArray.getString(i);
                list.add(value);
            }
        }

        while (list.size() < length) {
            list.add(filler);
        }

        return list;
    }
}

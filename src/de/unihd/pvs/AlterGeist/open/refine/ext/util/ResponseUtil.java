package de.unihd.pvs.AlterGeist.open.refine.ext.util;

import org.json.JSONException;
import org.json.JSONWriter;

public class ResponseUtil {

    public static void appendMatrixToWriter(String key, String[][] matrix, JSONWriter writer) throws JSONException {
        writer.key(key);
        writer.array();
        for (String[] row : matrix) {
            writer.array();
            for (String cell : row) {
                StringBuffer sb = new StringBuffer();
                sb.append(cell);
                writer.value(sb.toString());
            }
            writer.endArray();
        }
        writer.endArray();
    }

    public static void appendArrayToWriter(String key, String[] array, JSONWriter writer) throws JSONException {
        writer.key(key);
        writer.array();
        for (String cell : array) {
            StringBuffer sb = new StringBuffer();
            sb.append(cell);
            writer.value(sb.toString());
        }
        writer.endArray();
    }

}

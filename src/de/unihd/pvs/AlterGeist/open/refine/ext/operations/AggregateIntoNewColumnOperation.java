package de.unihd.pvs.AlterGeist.open.refine.ext.operations;

import com.google.refine.model.Project;
import com.google.refine.operations.EngineDependentOperation;
import de.unihd.pvs.AlterGeist.open.refine.ext.Aggregator;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONWriter;

import java.util.Properties;

public class AggregateIntoNewColumnOperation extends EngineDependentOperation {
    private String columnName;
    private Project project;
    private Aggregator aggregator;

    public AggregateIntoNewColumnOperation(JSONObject engineConfig, Project project, String columnName, String inputValues) {
        super(engineConfig);
        this.columnName = columnName;
        this.project = project;

//        this.aggregator = new Aggregator(project, inputValues, -1);
    }

    @Override
    public void write(JSONWriter writer, Properties properties) throws JSONException {

    }
}

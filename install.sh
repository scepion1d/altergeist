#!/bin/sh
RefinePath=${1:-~/Workspace/OpenRefine}

ExtensionDir=alter-geist

if [ -d $RefinePath/extensions/$ExtensionDir ]; then
    rm -rf $RefinePath/extensions/$ExtensionDir
fi

mkdir $RefinePath/extensions/$ExtensionDir

cp -r ./module $RefinePath/extensions/$ExtensionDir/module

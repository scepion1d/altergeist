# Extension for OpenRefine
## About
Vary imortant information about extension
## Instalation
    /<path-to-extension-root>/install.sh <path-to-open-refine-root>

## Development
### Dependencies
1. OpenRefine classes
    /<path-to-open-refine-root>/main/webapp/WEB-INF/classes
2. OpenRefine dependencies
    /<path-to-open-refine-root>/main/webapp/WEB-INF/lib
3. Java EE

### WARNING
**Don't forget to build .class files and place it into "_<path-to-extension-root>/module/MOD-INF/classes_" after any changes sources of server side part**
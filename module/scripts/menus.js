/* Add menu to extension bar */
ExtensionBar.addExtensionMenu({
    id: "alter-geist",
    label: "AlterGeist",
    submenu: [
        {
            id: "alter-geist/aggregate",
            label: "Aggregate",
            click: dialogHandler(AggregationPreviewDialog),
        },
        { /* separator */ },
        {
            id   : "alter-geist/about",
            label: "About",
            click: dialogHandler(AboutDialog),
        },
    ]
});

/* Add submenu to column header menu */
//DataTableColumnHeaderUI.extendMenu(function (column, columnHeaderUI, menu) {
//    MenuSystem.insertAfter(menu, "core/view", [
//        {
//            id: "alter-geist/aggregate",
//            label: "Aggregate",
//            click: dialogHandler(AggregateDialog, column),
//        },
//        { /* separator */ },
//    ]);
//});

function dialogHandler(dialogConstructor) {
    var dialogArguments = Array.prototype.slice.call(arguments, 1);

    function Dialog() {
        return dialogConstructor.apply(this, dialogArguments);
    }

    Dialog.prototype = dialogConstructor.prototype;

    return function () {
        new Dialog().show();
    };
}

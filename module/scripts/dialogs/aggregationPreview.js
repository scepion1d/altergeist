// Dialog constructor
function AggregationPreviewDialog() {}

// Dialog init
AggregationPreviewDialog.prototype.show = function() {
    var self = this;

    // Load dialog HTML template
    this.dialogElement = $(
        DOM.loadHTML("alter-geist", "templates/dialogs/aggregationPreview.html")
        // Inject expression preview window into template
        .replace("$AGGREGATION_PREVIEW_WIDGET$", AggregationPreviewDialog.getBaseHtml())
    );

    // Get elements of dialog
    this.elements = DOM.bind(this.dialogElement);

    // Set dialog header
    this.elements.dialogHeader.text("Aggragate project data into new column");

    // Show dialog
    // WARNING: Must be before preview widget init
    this.dialogLevel = DialogSystem.showDialog(this.dialogElement);

    // Init expression preview windows and share it
    window.aggregationPreviewWidget = new AggregationPreviewDialog.Widget (
        this.elements,
        this.elements.dialogBody.width()
    );

    // Bind actions to controls
    this.elements.cancelBtn.click(function () {
        DialogSystem.dismissUntil(self.dialogLevel - 1);
    });
    this.elements.clearBtn.click(function () {
        window.aggregationPreviewWidget.clearInput(self.dialogElement);
    });
    this.elements.previewBtn.click(function () {
        window.aggregationPreviewWidget.render();
    });
    this.elements.aggregateBtn.click(function () {
        alert("Some actions will be here!");
        DialogSystem.dismissUntil(self.dialogLevel - 1);
    });
}



// Get base HTML code of preview widget
AggregationPreviewDialog.getBaseHtml = function() {
    html = DOM.loadHTML("alter-geist", "templates/widgets/aggregationPreview.html");
    return html;
};

// Init widget
AggregationPreviewDialog.Widget = function(elements, widgetWidth) {
    this.elements = elements;
    this.contentWidth = widgetWidth;
    this.inputWidth = 192;
    this.spacerWidth = 15;

    this.inputValues = [];
    this.onChangeInput(0, "New column name");

    this.render();
    return this;
};

// Replace 'undefined' values in array with specified default value
AggregationPreviewDialog.Widget.prototype.replaceUndefinedValues = function(array, value) {
    if(typeof array === 'undefined') {
       return [];
    }

    for (var i = 0; i < array.length; i++) {
        if (typeof array[i] === 'undefined') {
            array[i] = value;
        }
    }

    return array;
};

// Render preview widget
AggregationPreviewDialog.Widget.prototype.render = function() {
    var self = this;
    var params = {
        project: theProject.id,
        rowsNum: theProject.rowModel.limit,
        firstRow: theProject.rowModel.start,
        inputValues: JSON.stringify(
                        this.replaceUndefinedValues(
                            this.inputValues.slice(1, this.inputValues.length),
                            ""
                        )
                     )
    };

    $.post(
        "command/alter-geist/preview-aggregated-data?" + $.param(params),
        null,
        function(response) {
            if (response.code != "error") {
                self.renderPreviewProject();
                self.renderPreviewInput(response.aggregatedData);
            } else {
                alert("Something went wrong!\n" + response.message);
            }
        },
        "json"
    );
};

// Render project table
AggregationPreviewDialog.Widget.prototype.renderPreviewProject = function() {
    var container = this.elements.aggregationPreviewProject.empty().width(this.contentWidth - this.inputWidth - this.spacerWidth);
    var table = $('<table id="aggregation-preview-project-table" class="data-table aggregation-preview-table"></table>').appendTo(container)[0];

    // Add table header to widget
    var head = $('<thead></thead>').appendTo(table)[0];
    var tr = head.insertRow(0);
    $(tr.insertCell(0)).addClass("expression-preview-heading").text("row");
    for (var i = 0; i < theProject.columnModel.columns.length; i++) {
        $(tr.insertCell(i + 1)).addClass("expression-preview-heading").text(theProject.columnModel.columns[i].name);
    }

    var body = $('<tbody></tbody>').appendTo(table)[0];
    for (var i = 0; i < theProject.rowModel.rows.length; i++) { // rows
        var tr = body.insertRow(i);

        // add row number as first column
        $(tr.insertCell(0)).attr("width", "1%").html((theProject.rowModel.start + i + 1) + ".");

        for (var j = 0; j < theProject.rowModel.rows[i].cells.length; j++) { // columns
            var tdValue = $(tr.insertCell(j + 1)).addClass("aggregation-preview-value");
            if (theProject.rowModel.rows[i].cells[j] !== null) {
                this.addValue(tdValue, theProject.rowModel.rows[i].cells[j].v);
            }
        }
    }

    // synchronise scrolling with input
    $("#aggregation-preview-project").scroll(function () {
        $("#aggregation-preview-input").scrollTop($("#aggregation-preview-project").scrollTop());
    });
};

// Render input for new column
AggregationPreviewDialog.Widget.prototype.renderPreviewInput = function(aggregatedData) {
    var container = this.elements.aggregationPreviewInput.empty().width(this.inputWidth);
    var table = $('<table id="aggregation-preview-input-table" class="data-table aggregation-preview-table aggregation-preview-input"></table>').appendTo(container)[0];

    var tr = table.insertRow(0);
    $(tr.insertCell(0)).addClass("aggregation-preview-heading")
                       .html(DOM.loadHTML("alter-geist", "templates/widgets/aggregationPreviewInput.html")
                                .replace("$AGGREGATION_PREVIEW_WIDGET_NEW_COLUMN_ROW_NUM$", 0)
                                .replace("$AGGREGATION_PREVIEW_WIDGET_NEW_COLUMN_VALUE$", this.inputValues[0]));


    for (var i = 0; i < aggregatedData.length; i++) {
        var tr = table.insertRow(table.rows.length);
        var text = aggregatedData[i];
        if (typeof text == 'undefined' || text === null) {
            text = "";
        }

        var rowInput = DOM.loadHTML("alter-geist", "templates/widgets/aggregationPreviewInput.html")
                          .replace("$AGGREGATION_PREVIEW_WIDGET_NEW_COLUMN_ROW_NUM$", i + 1)
                          .replace("$AGGREGATION_PREVIEW_WIDGET_NEW_COLUMN_VALUE$", text);
        $(tr.insertCell(0)).html(rowInput);
    }
    // synchronise scrolling with project preview
    $("#aggregation-preview-input").scroll(function () {
        $("#aggregation-preview-project").scrollTop($("#aggregation-preview-input").scrollTop());
    });
};

AggregationPreviewDialog.Widget.prototype.addValue = function(td, v) {
    if (v !== null && v !== undefined) {
        if ($.isPlainObject(v)) {
            $('<span></span>').addClass("expression-preview-special-value").text($.i18n._('core-dialogs')["error"]+": " + v.message).appendTo(td);
        } else {
            td.text(v);
        }
    } else {
        $('<span>null</span>').addClass("expression-preview-special-value").appendTo(td);
    }
};

// Remove all values from input
AggregationPreviewDialog.Widget.prototype.clearInput = function(dialog) {
    var columnName = this.inputValues[0];
    this.inputValues = [];
    this.inputValues[0] = columnName;

    var inputs = dialog.find("input.aggregation-preview-input");
    inputs.splice(0, 1); // remove column name input
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].value = "";
    }
};

AggregationPreviewDialog.Widget.prototype.onChangeInput = function(id, value) {
    this.inputValues[id] = value;
};

function onChangeInput(id, value) {
    window.aggregationPreviewWidget.onChangeInput(id, value);
};

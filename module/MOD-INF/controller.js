var logger = Packages.org.slf4j.LoggerFactory.getLogger("alter-geist");
var AlterGeist = Packages.de.unihd.pvs.AlterGeist.open.refine.ext;
var commands = AlterGeist.commands;

/* Initialize the extension. */
function init() {
    logger.info("Initializing server side resources");
    this.registerCommands();

    logger.info("Initializing client side resources");
    var resourceManager = Packages.com.google.refine.ClientSideResourceManager;

    this.loadJs(resourceManager);
    this.loadLess(resourceManager);
}

function register(path, command) {
  var refineServlet = Packages.com.google.refine.RefineServlet;
  refineServlet.registerCommand(module, path, command);
}

function registerCommands() {
    logger.info("    Initializing commands...");
    this.register("preview-aggregated-data", new commands.AggregationPreviewCommand());
}

function loadJs(resourceManager) {
    logger.info("    Initializing scripts...");
    resourceManager.addPaths(
        "project/scripts",
        module, [
            // Add system scripts
            "scripts/config.js", // Should be first
            "scripts/util.js",  // Should be before scripts/menus.js
            // Add ui scripts
            "scripts/dialogs/about.js",
            "scripts/dialogs/aggregationPreview.js",
            "scripts/menus.js", // Should be loaded after all dialogs and widgets
        ]
    );
}

function loadLess(resourceManager) {
    logger.info("    Initializing styles...");
    resourceManager.addPaths(
        "project/styles",
        module, [
            "styles/main.less",
            "styles/dialogs/about.less",
            "styles/dialogs/aggregationPreview.less",
            "styles/widgets/aggregationPreview.less",
        ]
    );
}